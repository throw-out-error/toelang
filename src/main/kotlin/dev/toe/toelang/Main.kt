import dev.toe.toelang.LanguageParser
import dev.toe.toelang.api.LibraryRegistry
import dev.toe.toelang.api.LogLibrary
import java.io.File

fun main(args: Array<String>) {
    LibraryRegistry.register(LogLibrary())
    LanguageParser().parse(File(args[0]).readText(Charsets.UTF_8))
}
