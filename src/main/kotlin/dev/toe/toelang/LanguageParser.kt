package dev.toe.toelang

class LanguageParser {
    private val printRegex = Regex("^\\s*print\\s*(?:\\(\"(.*)\"\\))")
    private val varRegex = Regex("var\\s(\\w+)\\s=\\s(?:\"?)([^\"]+)(?:\"?)")
    private val fnRegex = Regex("(\\w?.+)\\s*\\(\\s*\"?(.*?)\"?\\s*\\)\\s*")

    fun parse(code: String) {
        val lines = code.lines()
        // TODO: error checking with column number
        // var col = 0

        val executor = LanguageExecutor()
        val expected = mutableListOf<String>()
        expected.addAll(mutableListOf("print", "var"))

        lines.forEachIndexed { lineNum, line ->
            if (line.isNotEmpty()) {
                if (line.contains("var") && varRegex.matches(line)) {
                    val res = varRegex.find(line)
                    if (res?.groups != null) {
                        val args = res.groups.map { g -> g?.value }.toMutableList().apply {
                            removeAt(0)
                        }
                        if (args[0] != null && args[1] != null)
                            executor.variableStore[args[0]!!] = args[1]!!
                    }
                } else if (fnRegex.matches(line)) {
                    val res = fnRegex.find(line)
                    if (res != null && res.groups[0] != null) {
                        val args = res.groups.map { g -> g?.value }.toMutableList().apply {
                            removeAt(0)
                        }
                        executor.execute(
                            args[0]!!,
                            args.apply {
                                removeAt(0)
                            }
                        )
                    }
                } else {
                    println("Error on line ${lineNum + 1}; Expected ${expected.joinToString(",")}")
                }
            }
        }
    }
}
