package dev.toe.toelang

import dev.toe.toelang.api.LibraryRegistry

class LanguageExecutor {
    val variableStore = mutableMapOf<String, Any>()

    fun execute(input: String, inputParams: List<Any?>) {
        val splitString = input.split(".")
        if (splitString.size >= 2) {
            val libName = splitString[0]
            val cmdName = splitString[1]

            val params = inputParams.map {
                if (it is String)
                    if (variableStore.containsKey(it))
                        variableStore[it]
                    else
                        it
                else
                    it
            }

            LibraryRegistry.get(libName).map { lib ->
                val arr = lib.create()

                arr.find {
                    it.name == cmdName
                }?.actionFn?.apply(params)
            }.orElse {
                println("Invalid library $libName")
            }
        }
    }
}
