package dev.toe.toelang.api

import java.util.function.Function

typealias ActionFn = Function<List<Any?>, Any?>

class CommandBuilder {
    var actionFn: ActionFn = Function {}
        private set
    var name: String = ""

    private val parameters = mutableMapOf<String, CommandParameter>()

    fun param(init: CommandParameter.() -> Unit): CommandParameter {
        val p = CommandParameter()
        p.init()
        parameters[p.name] = p
        return p
    }

    fun action(init: ActionFn): CommandBuilder {
        this.actionFn = init
        return this
    }

    companion object {
        @JvmStatic
        fun command(init: CommandBuilder.() -> Unit): CommandBuilder {
            val cmd = CommandBuilder()
            cmd.init()
            return cmd
        }
    }

    class CommandParameter {
        var name: String = ""
        var type: String = ParameterType.STRING

        object ParameterType {
            const val STRING = "string"
        }
    }
}
