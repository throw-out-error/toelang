package dev.toe.toelang.api

import dev.toe.toelang.api.CommandBuilder.Companion.command

class LogLibrary : ILibrary {
    override fun create(): Array<CommandBuilder> {
        return arrayOf(
            command {
                name = "print"

                param {
                    name = "message"
                    type = "string"
                }

                action {
                    println(it.joinToString(" "))
                }
            }
        )
    }

    override fun getModuleName(): String {
        return NAME
    }

    companion object {
        val NAME = "Log"
    }
}
