package dev.toe.toelang.api

import java.util.Optional

object LibraryRegistry {
    private val libraries: MutableMap<String, ILibrary> = mutableMapOf()

    fun register(lib: ILibrary) {
        libraries[lib.getModuleName()] = lib
    }

    fun get(name: String): Optional<ILibrary> {
        return Optional.ofNullable(libraries[name])
    }
}
