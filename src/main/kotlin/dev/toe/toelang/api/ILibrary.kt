package dev.toe.toelang.api

interface ILibrary {
    fun getModuleName(): String
    fun create(): Array<CommandBuilder>
}
